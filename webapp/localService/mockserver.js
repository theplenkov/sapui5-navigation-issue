sap.ui.define([
	"sap/ui/core/util/MockServer"
], function (MockServer) {
	"use strict";
	return {
		init: function () {
			// create
			var oMockServer = new MockServer({
				rootUri: "/"
			}); 
			// simulate
			oMockServer.simulate("../localService/metadata.xml",{
				//sMockdataBaseUrl: "../localService/mockdata",
				bGenerateMissingMockData: true
			});
			
			// start
			oMockServer.start();
		}
	};
});